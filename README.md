# webpack reactjs app

A barebones ReactJS application with a custom webpack and babel configuration. Created for the educational purpose of spinning up a React app without the create-react-app cli tool. 
Can be forked to start new ReactJS projects with little bloat.