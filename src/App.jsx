import React from 'react'
import { hot } from 'react-hot-loader'

class App extends React.Component {
  render() {
    return (
      <div className='App'>
        <p>Hello world! I'm</p>
        <h1>Gavin Vaught</h1>
        <p>& this is my [basic] React App</p>
      </div>
    )
  }
}

export default hot(module)(App)
